﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TrMensageriaSend.Models;
using enviaRabbit;

namespace TrMensageriaSend.Controllers
{
    public class HomeController : Controller
    {
       public ActionResult Index(form aux) {
            if(!string.IsNullOrEmpty(aux.mensagem))
            {
                string m = string.Join(" ", aux.mensagem);
                enviar(m);
            }
            return View();
        }

        public void enviar(string aux)
        {
            EnviarMensagem send = new EnviarMensagem();
            send.sendMessage(aux);
        }
    }
}